<?php
include('register-controller.php');
include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link
      rel="stylesheet"
      href="./node_modules/bootstrap/dist/css/bootstrap.min.css"
    />
    <link rel="stylesheet" href="./node_modules/flipClock/css/flipclock.css" />
    <link rel="stylesheet" href="./node_modules/chosen-js/chosen.min.css" />
    <link rel="stylesheet" href="./node_modules/tooltipster/dist/css/tooltipster.bundle.min.css">
    <link rel="stylesheet" href="./node_modules/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css">
    <link rel="stylesheet" href="./node_modules/pretty-checkbox/dist/pretty-checkbox.min.css">

    <link rel="stylesheet" href="./style/style.css" />

    <title>Create account</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 id="title" class="text-center"> Create Acount </h2>
          <h5 class="text-center">already a member?
            <a href="/<?php echo $project_name ?>/login.php">Login</a>
          </h5>
        </div>
      </div>

      <div class="row">
        <!-- page right side -->
        

        <!-- page left side -->
        <div class="col-6">
          <form id="signup" method="POST">
          <?php include('errors.php'); ?>
                <!-- first line: name and family name -->
                <div class="form-line">
                  <!-- first line label -->
                  <div class="row">
                    <div class="col">
                      <label for="first_name"> Name </label>
                    </div>
                  </div>
                  <!-- first line inputs -->
                  <div class="row ">
                    <div class="col">
                      <input
                        type="text"
                        name="first_name"
                        id="first_name"
                        placeholder="First"
                        data-validation="required"
                        data-validation-error-msg="فیلد را نمی توانید خالی رها کنید!"
                      />
                    </div>
                    <div class="col">
                      <input
                        type="text"
                        name="last_name"
                        id="last_name"
                        placeholder="Last"
                        data-validation="required"
                        data-validation-error-msg="فیلد را نمی توانید خالی رها کنید!"
                      />
                    </div>
                  </div>
                </div>
      
                <!-- second line: username -->
                <div class="form-line">
                  <!-- second line label -->
                  <div class="row">
                    <div class="col">
                      <label for="username"> Choose your username </label>
                    </div>
                  </div>
      
                  <!-- second line input -->
                  <div class="row">
                    <div class="col">
                      <input
                        type="text"
                        name="username"
                        id="username"
                        data-validation="required length custom" data-validation-length="min8"
                        data-validation-regexp="^([a-zA-Z0-9\_]+.?)+$"
                        title=" شما تنها مجاز به استفاده از حروف انگلیسی و ارقام و نقطه هستید! "
                        data-validation-error-msg="نام کاربری معتبر نیست!"
                        placeholder="Username"
                      />
                    </div>
                  </div>
                </div>
      
                <!-- third line: password -->
                <div class="form-line">
                  <!-- third line label -->
                  <div class="row">
                    <div class="col"><label for="password"> Create a password </label></div>
                  </div>
      
                  <!-- third line input -->
                  <div class="row">
                    <div class="col">
                      <input type="password" 
                             title=" طول کلمه ی عبور می بایست حداقل 8 کاراکتر باشد. لطفا از کلمه ی عبور سایت های دیگر همچنین کلمات ساده استفاده نکنید. " 
                             name="password" id="password"
                             data-validation="length" data-validation-length="min8"
                             data-validation-error-msg="حداقل 8 کاراکتر باید وارد کنید!" />
                    </div>
                  </div>
                </div>
      
                <!-- forth line: repeat password -->
                <div class="form-line">
                  <!-- forth line label -->
                  <div class="row">
                    <div class="col">
                      <label for="repeat_password"> Confirm your password </label>
                    </div>
                  </div>
      
                  <!-- forth line input -->
                  <div class="row">
                    <div class="col">
                      <input
                        type="password"
                        name="repeat_password"
                        id="repeat_password"
                        data-validation="length" data-validation-length="min8"
                        data-validation-error-msg="حداقل 8 کاراکتر باید وارد کنید!"
                      />
                    </div>
                  </div>
                </div>
      
                <!-- fifth line: Birth date -->
                <div class="form-line">
                  <div class="row">
                    <div class="col">
                      <label for="birth_day"> Birthday </label>
                    </div>
                  </div>
      
                  <div class="row">
                  <div class="col">
                      <select
                        name="month"
                        data-placeholder=" month "
                        class="chosen-select"
                        data-validation="required"
                      >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      </select>
                    </div>                    
                    /
                    <div class="col">
                      <input type="number" name="day" id="day" placeholder=" day " min="1" max="31"
                      data-validation="required number"
                      data-validation-allowing="range[1;31]"
                        data-validation-error-msg="فیلد را نمی توانید خالی رها کنید یا عدد خارج از بازه ی 1تا 31 وارد کنید! " />
                    </div>
                    /
                    <div class="col">
                      <input type="number" name="year" id="year" placeholder=" year " min="1320" max="1397"
                      data-validation="required number"
                      data-validation-allowing="range[1320;1397]"
                        data-validation-error-msg="فیلد را نمی توانید خالی رها کنید یا عدد خارج از بازه ی 1320تا 1397 وارد کنید! "/>
                    </div>
                  </div>
                </div>
      
                <!-- sixth line: gender, phone number -->
                <div class="form-line">
                  <div class="row">
                    <div class="col-4">
                      <label for="gender"> Gender </label>
                    </div>
                  </div>
      
                  <div class="row">
                    <div class="col-4">
                      <select
                        name="gender"
                        data-placeholder="..."
                        class="chosen-select"
                        data-validation="required"
                      >
                        <option value="m"> male </option>
                        <option value="f"> female </option>
                      </select>
                    </div>
                    
                  </div>
                </div>
      
                <!-- seventh and eighth line: accept conditions & submit -->
                <div class="form-line">
                <div class="row form-line">
              <img src="captcha.php" />
              <input type="text" name="phrase" />
            </div>
                  <div class="row">
                    <div class="col">
                      <input type="checkbox" name="accept" id="accept" >
                      <label for="accept"> I agree with the terms of the services ... </label>
                    </div>
                  </div>
      
                  <div class="row">
                    <div class="col">
                      <button type="submit" id="submit" name="register" disabled class="btn btn-primary"> Register </button>
                    </div>
                </div>
              </div>
            </form>
            </div>

            
      <!-- .row (main) -->
    </div>
    <!-- .container -->

    <!-- scripts -->
  </body>
  <script src="./node_modules/jquery/dist/jquery.min.js"></script>
  <script src="./node_modules/chosen-js/chosen.jquery.min.js"></script>
  <script src="./node_modules/flipClock/compiled/flipclock.min.js"></script>
  <script src="./node_modules/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
  <script src="node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js"></script>
  <script src="./node_modules/sweetalert/dist/sweetalert.min.js"></script>
  <script src="./script/clock.js"></script>
  <script src="./script/chosen.js"></script>
  <script src="./script/tooltipster.js"></script>
  <script src="./script/validator.js"></script>
</html>
