<?php
// database stuff like connection and queries are written here
$db_name = 'project3_94463114';
$host_name = 'localhost';
$db = mysqli_connect($host_name, 'root', '', $db_name);

function check_user_exists( $username, $table_name = 'users' ){
    return "SELECT * FROM $table_name WHERE username='$username' LIMIT 1";
}


function insert_user_query( $username, $first_name, $last_name, $password, $gender, $birthDate, $table_name = 'users' ){
    return "INSERT INTO $table_name (username, first_name, last_name, password, gender, birthDate) 
                VALUES('$username', '$first_name', '$last_name', '$password', '$gender' , '$birthDate')";
}

function insert_wrong_pass_query( $username, $first_name, $last_name, $password, $gender, $birthDate, $table_name = 'wrong_passwords' ){
    return "INSERT INTO $table_name (username, first_name, last_name, password, gender, birthDate) 
                VALUES('$username', '$first_name', '$last_name', '$password', '$gender' , '$birthDate')";
}

function select_user_byID_query( $user_ID, $table_name = 'users' ){
    return "SELECT * FROM $table_name 
                WHERE id = $user_ID";
}

function select_by_username_query( $username, $table_name = 'users' ){
    return "SELECT * FROM $table_name WHERE username='$username' LIMIT 1";
}