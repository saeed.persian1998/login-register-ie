<?php
include('config.php');
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link
      rel="stylesheet"
      href="./node_modules/bootstrap/dist/css/bootstrap.min.css"
    />

    <link rel="stylesheet" href="./style/style.css" />
    <link rel="stylesheet" href="./style/login.css" />
    <title>Login</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
          <div id="wrapper">
          <div class="row form-line">
              <div class="col">
                <h4 class="text-center">Your new password is:</h4>
                <div id="random-password">
                  <strong id="random-password"><?php echo generateRandomString(16) ?></strong>
                </div>
            </div>
          </div>

          <div class="row form-line">
              <div class="col">
                <a href="/<?php echo $project_name ?>/signup.php" class="btn-full btn btn-primary">Register</a>
              </div>
              <div class="col">
                <a href="/<?php echo $project_name ?>/login.php" class="btn btn-primary btn-full">Login</a>
              </div>
            </div>
          </div>
        </div>
            </div>
        <div class="col-3"></div>
      </div>
    </div>
  </body>
</html>
