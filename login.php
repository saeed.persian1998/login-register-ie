<?php
include('login-controller.php');
include('config.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link
      rel="stylesheet"
      href="./node_modules/bootstrap/dist/css/bootstrap.min.css"
    />

    <link rel="stylesheet" href="./style/style.css" />
    <link rel="stylesheet" href="./style/login.css" />
    <title>Login</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
          <div id="wrapper">
            <form method="POST">
            <?php include('errors.php'); ?>

            <div class="row form-line">
              <div class="col">
                <!-- <input type="text" name="phrase" /> -->
                <input type="text" name="username" placeholder="Username" />
              </div>
            </div>

            <div class="row form-line">
              <div class="col">
                <input type="password" name="password" placeholder="Password" />
              </div>
            </div>

            <div class="row form-line">
              <div class="col">
                <input type="checkbox" id="remember_me" name="remember_me" />
                <label for="remember_me">Remember me</label> 
              </div>
            </div>

            <div class="row form-line">
              <div class="col">
                <a href="/<?php echo $project_name ?>/forgetPassword.php">Forget your password?</a>
              </div>
            </div>
            <?php if( isset($_SESSION['is_wrong']) && $_SESSION['is_wrong'] ): ?>
            <div class="row form-line">
              <img src="captcha.php" />
              <input type="text" name="phrase" />
            </div>
            <?php endif; ?>
            
            <div class="row form-line">
              <div class="col">
                <a href="/<?php echo $project_name ?>/signup.php" class="btn-full btn btn-primary ">Register</a>
              </div>
              <div class="col">
                <button type="submit" name="login" class="btn btn-primary btn-full">Login</button>
              </div>
            </div>
            </form>
          </div>
        </div>
            </div>
        <div class="col-3"></div>
      </div>
    </div>
  </body>
</html>
