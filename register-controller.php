<?php
session_start();

include('DB.php');
include('parameters.php');

if(!isset($_SESSION['phrase'])){
  $_SESSION['phrase'] = '';
  $_POST['phrase'] = '';
}

if (isset($_POST['register'])){
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $first_name = mysqli_real_escape_string($db, $_POST['first_name']);
  $last_name = mysqli_real_escape_string($db, $_POST['last_name']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $repeat_password = mysqli_real_escape_string($db, $_POST['repeat_password']);
  $gender = mysqli_real_escape_string($db, $_POST['gender']);
  $day = mysqli_real_escape_string($db, $_POST['day']);
  $month = mysqli_real_escape_string($db, $_POST['month']);
  $year = mysqli_real_escape_string($db, $_POST['year']);
  $birthDate = (string)$year . "/" . (string)$month . "/" . (string)$day;


  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($password)) { array_push($errors, "Password is required"); }
  if ($password != $repeat_password) {
      array_push($errors, "The two passwords do not match");
  }
    
  $result = mysqli_query($db, check_user_exists($username));
  $user = mysqli_fetch_assoc($result);
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }
  }


  if($_SESSION['phrase'] !== ''){
    if(isset($_SESSION['phrase']) && $_SESSION['phrase'] === $_POST['phrase']){
      // Finally, register user if there are no errors in the form
      if (count($errors) == 0) {
        $password_hash = md5($password);//encrypt the password before saving in the database

        $query = insert_user_query( $username, $first_name, $last_name, $password_hash, $gender, $birthDate );
        if(mysqli_query($db, $query)){
          $_SESSION['user_id'] = $username;
          $_SESSION['success'] = "You are now logged in";
          header('location: home.php');
        }
      }
    }else{
      array_push($errors, "Wrong captcha!");
    }
  }else{
    array_push($errors, "Captcha could not be empty!");
  }
  
  
}
