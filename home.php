<?php 
session_start();
include('DB.php');
include('config.php');
include('parameters.php');

// setcookie('user_id','', time() - 5);
if (!isset($_SESSION['user_id'])){
    header('location: login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
}

$query = select_user_byID_query($_SESSION['user_id']);
$result = mysqli_query($db, $query);

if($result){
    $user = mysqli_fetch_assoc($result);
}else{
    $query = select_by_username_query($_SESSION['user_id']);
    $result = mysqli_query($db, $query);
    $user = mysqli_fetch_assoc($result);
}
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link
      rel="stylesheet"
      href="./node_modules/bootstrap/dist/css/bootstrap.min.css"
    />

    <link rel="stylesheet" href="./style/home.css" />
    <title>Home</title>
</head>
<body>
    <div class="container">
        <div id="wrapper">
        <div class="row">
            <div class="col">
                <h3>Welcome <?php echo $user['first_name'] ?>!</h3>
            </div>
        </div>
        <ul>
        <?php foreach ($user as $key => $value){ 
            if(($key != 'password') && ($key != 'id')){?>

        <div class="row">
            <div class="col">
                <li> <?php echo $key . ": "; echo $value; ?></li>
            </div>
        </div>
        <?php }}  ?>
        </ul>
        <div class="row">
            <div class="col">
                <img src="./assets/mikky.png" style="height:15rem;"/>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a class="btn btn-danger" id="logout" href="/<?php echo $project_name ?>/home.php?logout='1'">
                    Logout
                </a>    
            </div>
        </div>
        </div>
    </div>
    
</body>
</html>