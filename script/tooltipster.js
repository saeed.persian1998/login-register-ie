$("#username").tooltipster({
  theme: "punk",
  side: "bottom",
  bottom: ["bottom", "top", "right", "left"],
  trigger: "click"
});

$("#password").tooltipster({
  theme: "punk",
  side: "bottom",
  bottom: ["bottom", "top", "right", "left"],
  trigger: 'click'
});

$("#username").focus(function() {
  $(this).tooltipster("open");
}).blur(function(){
   $(this).tooltipster('close');
});

$('#password')
.focus(function(){
    $(this).tooltipster('open');
})
.blur(function(){
   $(this).tooltipster('close');
});