$(document).ready(function() {
  var clock;

  clock = $(".clock").FlipClock({
    clockFace: "HoursCounter",
    autoStart: false,
    callbacks: {
      stop: function() {
        $(".message").html("زمان شما  برای ثبت نام به پایان رسید!!"),
          $(".ic").attr("disabled", "disabled"),
          $(".ic").css("cursor", "no-drop"),
          $(".sub").attr("disabled", "disabled"),
          $(".sub").css("background-color", "red"),
          $(".sub").css("cursor", "no-drop");
      }
    }
  });

  clock.setTime(3600);
  clock.setCountdown(true);
  clock.start();
});
