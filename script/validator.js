$.validate({
  form: '#signup',
  modules: 'security',
  onError : function($form) {
    alert('Validation of form '+$form.attr('id')+' failed!');
  },
  oSuccess: function() {
    alert('s');
  },
  onValidate : function($form) {
    return {
      element : $('#password'),
      message : 'This input has an invalid value for some reason'
    }
  }
});

// Restrict presentation length
$("#presentation").restrictLength($("#pres-max-length"));

$.validate({
  borderColorOnError: "#aa0000",
  addValidClassOnAll: true
});

$('#accept').change(function(ev) {
  if ( $('#accept').is(':checked') ) {
    $('#submit').removeAttr('disabled');
  }
  else {
    $('#submit').prop("disabled",true);
  }
});

$('#signup').on('Error', function() {
  alert('sss');
});