<?php
session_start();

include('DB.php');
include('parameters.php');

if(!isset($_SESSION['loginAttemp'])){
  $_SESSION['loginAttemp']=0;
  $_SESSION['phrase'] = '';
  $_POST['phrase'] = '';
}

if(!isset($_SESSION['is_wrong'])){
  $_SESSION['is_wrong']=false;
}

if (isset($_POST['login'])){
  getFormValue();
  
  
  //fetch user from database by username
  $result = mysqli_query($db, check_user_exists($username));
  $user = mysqli_fetch_assoc($result);
  // var_dump($_SESSION['is_wrong']);
  // die();
  if($password_notEmpty && $username_notEmpty && !$_SESSION['is_wrong']){
    if($user){// if user exist & did not inserted wrong password 3 times
      if($user["password"] == $password_hash){
        if (count($errors) == 0){ // if no errors at all
          login_user($user);
        }
      }elseif($password_notEmpty){
        password_is_wrong();
      }
    }else{
      array_push($errors, "User does not exists!");
    }
  }
  if($_SESSION['is_wrong']){ // captcha
    if($user){// if user exist & did not inserted wrong password 3 times
      if($user["password"] == $password_hash){
        if (count($errors) == 0){ // if no errors at all
          if($_SESSION['phrase'] !== ''){
            if(isset($_SESSION['phrase']) && $_SESSION['phrase'] === $_POST['phrase']){
              login_user($user);
            }else{
              array_push($errors, "Wrong captcha!");
            }
          }else{
            array_push($errors, "Captcha could not be empty!");
          }
        }
      }elseif($password_notEmpty){
        password_is_wrong();
      }
    }else{
      array_push($errors, "User does not exists!");
    }
  }
}
  

function getFormValue(){
  global $username, $password, $errors, $db, $password_hash;
  global $username_notEmpty, $password_notEmpty;
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $password_hash = md5($password);
  
  if (empty($username)) { array_push($errors, "Username is required"); $username_notEmpty = false; }
  if (empty($password)) { array_push($errors, "Password is required"); $password_notEmpty = false; }
}

function login_user($user){
  $_SESSION['user_id'] = $user['id'];
  $_SESSION['loginAttemp'] = 0;
  if(isset($_POST['remember_me'])){
    setcookie('remember_me', 1, time() + 10);
  }
  header('location: home.php');
}

function password_is_wrong(){
  global $errors;
  if(!$_SESSION['is_wrong']){
    array_push($errors, "Wrong password!");
  }
  $_SESSION['loginAttemp'] ++;
  if($_SESSION['loginAttemp'] >= 3){
    $_SESSION['is_wrong'] = true;
  }
}